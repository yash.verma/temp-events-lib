package types

import (
	"go.uber.org/zap"
)

// StandardLogger enforces specific log message formats
type StandardLogger struct {
	*zap.SugaredLogger
}
