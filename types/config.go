package types

import "time"

// BrokerConfig
type BrokerConfig struct {
	Brokers     []string
	MetricsAddr string
}

type ConsumerConfig struct {
	Name                   string
	Group                  string
	Topic                  string
	DlqTopic               string
	MaxRetries             int32
	InitialIntervalRetries time.Duration
}

type ProducerConfig struct {
	Name  string
	Topic string
}
