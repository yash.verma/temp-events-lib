package broker

import (
	"github.com/Shopify/sarama"
	"github.com/garsue/watermillzap"
	"gitlab.com/fampay/backend/events-lib/types"

	"github.com/ThreeDotsLabs/watermill-kafka/v2/pkg/kafka"
	"github.com/ThreeDotsLabs/watermill/message"
	wotel "github.com/voi-oss/watermill-opentelemetry/pkg/opentelemetry"
)

type watermillkafka struct {
	brokerConfig *types.BrokerConfig
	logger       *types.StandardLogger
}

func (k *watermillkafka) GetConsumer(config *types.ConsumerConfig) (message.Subscriber, error) {
	saramaSubscriberConfig := kafka.DefaultSaramaSubscriberConfig()
	saramaSubscriberConfig.Consumer.Offsets.Initial = sarama.OffsetOldest

	subscriber, err := kafka.NewSubscriber(
		kafka.SubscriberConfig{
			Brokers:               k.brokerConfig.Brokers,
			Unmarshaler:           DefaultMarshaler{},
			OverwriteSaramaConfig: saramaSubscriberConfig,
			ConsumerGroup:         config.Group,
		},
		watermillzap.NewLogger(k.logger.Desugar()),
	)
	return subscriber, err
}

func (k *watermillkafka) GetProducer(config *types.ProducerConfig) (message.Publisher, error) {
	publisher, err := kafka.NewPublisher(
		kafka.PublisherConfig{
			Brokers:   k.brokerConfig.Brokers,
			Marshaler: DefaultMarshaler{},
		},
		watermillzap.NewLogger(k.logger.Desugar()),
	)
	return wotel.NewNamedPublisherDecorator(config.Name, publisher), err
}

func NewWatermillKafkaBroker(config *types.BrokerConfig, logger *types.StandardLogger) Broker {
	return &watermillkafka{
		brokerConfig: config,
		logger:       logger,
	}
}
