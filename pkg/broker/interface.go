package broker

import (
	"github.com/ThreeDotsLabs/watermill/message"
	"gitlab.com/fampay/backend/events-lib/types"
)

type Broker interface {
	GetConsumer(*types.ConsumerConfig) (message.Subscriber, error)
	GetProducer(*types.ProducerConfig) (message.Publisher, error)
}
