package eventhandler

import (
	"encoding/json"
	"errors"

	"github.com/ThreeDotsLabs/watermill/message"
	"gitlab.com/fampay/backend/events-lib/types"
)

type eventHandler struct {
	logger           *types.StandardLogger
	handlerConfigMap HandlerConfigMap
}

func (eh *eventHandler) HandleEvent(msg *message.Message) error {
	be := BaseEvent{}
	err := json.Unmarshal(msg.Payload, &be)
	if err != nil {
		return err
	}
	hc, ok := eh.handlerConfigMap[be.Type]
	if !ok {
		return errors.New("handler not implemented")
	}
	dm, err := hc.Decoder(msg)
	if err != nil {
		return err
	}
	err = hc.Process(dm)
	return err
}

func NewEventHandler(logger *types.StandardLogger, handlerConfigMap HandlerConfigMap) eventHandler {
	eventHandlersObj := eventHandler{
		logger:           logger,
		handlerConfigMap: handlerConfigMap,
	}
	return eventHandlersObj
}
