package eventhandler

import "github.com/ThreeDotsLabs/watermill/message"

type BaseEvent struct {
	Type      string `json:"type"`
	Timestamp uint64 `json:"timestamp"`
}

type Decoder func(msg *message.Message) (interface{}, error)

type Process func(interface{}) error

// HandlerConfig ...
type HandlerConfig struct {
	Decoder Decoder
	Process Process
}

// HandlerConfigMap ...
type HandlerConfigMap map[string]*HandlerConfig
