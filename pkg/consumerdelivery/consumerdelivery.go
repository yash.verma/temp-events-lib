package consumerdelivery

import (
	"context"

	"github.com/ThreeDotsLabs/watermill/components/metrics"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/ThreeDotsLabs/watermill/message/router/middleware"
	"github.com/garsue/watermillzap"
	wotel "github.com/voi-oss/watermill-opentelemetry/pkg/opentelemetry"
	"gitlab.com/fampay/backend/events-lib/pkg/broker"
	"gitlab.com/fampay/backend/events-lib/pkg/eventhandler"
	"gitlab.com/fampay/backend/events-lib/types"
)

func NewConsumerDelivery(logger *types.StandardLogger, brokerConfig *types.BrokerConfig, consumerConfig *types.ConsumerConfig, handlerConfigMap eventhandler.HandlerConfigMap) error {
	// create a broker instance
	broker := broker.NewWatermillKafkaBroker(brokerConfig, logger)
	// create a router
	router, err := message.NewRouter(message.RouterConfig{}, watermillzap.NewLogger(logger.Desugar()))
	if err != nil {
		return err
	}

	// instantiate a prometheus registry and start listening
	prometheusRegistry, closeMetricsServer := metrics.CreateRegistryAndServeHTTP(brokerConfig.MetricsAddr)
	defer closeMetricsServer()

	metricsBuilder := metrics.NewPrometheusMetricsBuilder(prometheusRegistry, "", "")
	metricsBuilder.AddPrometheusRouterMetrics(router)

	// create a producer for dlq
	producer, err := broker.GetProducer(&types.ProducerConfig{
		consumerConfig.Name,
		consumerConfig.DlqTopic,
	})
	if err != nil {
		return err
	}

	producer, err = metricsBuilder.DecoratePublisher(producer)
	if err != nil {
		return err
	}

	// SignalsHandler will gracefully shutdown Router when SIGTERM is received.
	// You can also close the router by just calling `r.Close()`.
	// router.AddPlugin(plugin.S)

	// Router level middleware are executed for every message sent to the router
	// setup DLQ
	poisonHandler, err := middleware.PoisonQueue(producer, consumerConfig.DlqTopic)
	if err != nil {
		logger.Error(err)
		return err
	}

	router.AddMiddleware(
		wotel.Trace(),
		// CorrelationID will copy the correlation id from the incoming message's metadata to the produced messages
		middleware.CorrelationID,
		poisonHandler,
		// The handler function is retried if it returns an error.
		// After MaxRetries, the message is Nacked and it's up to the PubSub to resend it.
		middleware.Retry{
			MaxRetries:      int(consumerConfig.MaxRetries),
			InitialInterval: consumerConfig.InitialIntervalRetries,
			Logger:          watermillzap.NewLogger(logger.Desugar()),
		}.Middleware,

		// Recoverer handles panics from handlers.
		// In this case, it passes them as errors to the Retry middleware.
		middleware.Recoverer,
		func(h message.HandlerFunc) message.HandlerFunc {
			return func(message *message.Message) ([]*message.Message, error) {
				logger.Debugf("Consumed message: %s", message.UUID)
				r, e := h(message)
				if e != nil {
					logger.Debugf("Failed to process message: %s", message.UUID)
				} else {
					logger.Debugf("Successfully processed message: %s", message.UUID)
				}
				return r, e
			}
		},
	)

	// Initialize consumer
	consumer, err := broker.GetConsumer(consumerConfig)
	if err != nil {
		return err
	}
	consumer, err = metricsBuilder.DecorateSubscriber(consumer)
	if err != nil {
		return err
	}

	eventHandlers := eventhandler.NewEventHandler(logger, handlerConfigMap)
	router.AddNoPublisherHandler("event_handler", consumerConfig.Topic, consumer, eventHandlers.HandleEvent)
	ctx := context.Background()
	err = router.Run(ctx)
	return err
}
